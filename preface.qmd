---
engine: knitr
---

# Preface

## Download the Data Files

You can download the data files from [https://gitlab.com/raniere-phd/interactive-data-visualisation](https://gitlab.com/raniere-phd/interactive-data-visualisationhttps://gitlab.com/raniere-phd/interactive-data-visualisation).

## Conventions Used

Instructions to the command-line interface are set as follow:

```{bash}
#| eval: false

command --arg
```
