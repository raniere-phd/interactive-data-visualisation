#!/bin/bash
#
# After run
#
#     $ docker-compose up --force-recreate --build
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/interactive-data-visualisation"
docker compose build --no-cache --pull
docker image tag interactive-data-visualisation_web $DOCKER_IMAGE
docker push $DOCKER_IMAGE
